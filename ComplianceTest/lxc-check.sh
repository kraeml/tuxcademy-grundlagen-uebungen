#!/bin/bash

# Liste aller LXC-Container
containers=$(sudo lxc-ls)

# Schleife über alle gefundenen Container
for container in $containers; do
    # Überprüfen, ob der Container läuft
    sudo lxc-info -n $container | grep -q "RUNNING" 
    if [ $? -eq 0 ]; then
        echo "Container $container läuft."
        exit 0  # Wenn ein Container läuft, beende das Skript mit Exit-Code 0
    fi
done

# Kein laufender Container gefunden
echo "Kein Container läuft."
exit 1  # Kein laufender Container gefunden, beende das Skript mit Exit-Code 1
