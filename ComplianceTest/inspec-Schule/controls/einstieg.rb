# encoding: utf-8
# copyright: 2017, The Authors


title 'Basis Tests'

#ToDo Interfaces via Variablen
interfaces = {
  'enp0s3' => {
    'ip' => '10.0.2.15'
  },
  'enp0s8' => {
    'ip' => '192.168.33.10'
  },
  'docker0' => {
    'ip' => '172.17.0.1'
  },
  'lxcbr0' => {
    'ip' => '10.\d+.\d+.1'
  }
}

rootfs_paths = [
  "focal/rootfs-amd64"
]

control "base" do
  impact 1.0
  title 'Base Verzeichnisse und Dateien'
  desc 'Basis Test der Maschine'
  describe file('/home/vagrant/dateien') do
    it { should exist }
    it { should be_directory }
  end
  describe file('/home/vagrant/dateien/dnsmasq.conf.example') do
    it { should be_file }
  end
  #describe file('/home/vagrant/dateien/reguläre_Ausdrücke_-_Filter/frosch.txt') do
  #  it { should be_file }
  #end
  describe command('hostname') do
    its('stdout') { should match "rdf-dev" }
  end
  describe interface('enp0s3') do
    it { should be_up }
    #its('ipv4_addresses') { should include '10.0.2.15' }
  end
  describe bash('lxc-ls --fancy') do
    its('stdout') { should match "RUNNING" }
  end
  describe interface('lxcbr0') do
    it { should be_up }
  end
  interfaces.each do | interface, config|
    describe bash('ip a s ' + interface) do
      its('stdout') { should match config['ip'] }
    end
  end
  describe file('/etc/vagrant_box_build_date') do
    it { should be_file }
  end
  describe file('/etc/vagrant_box_version') do
    it { should be_file }
    its('content') { should match '\d+\.\d+\.' }
  end
  rootfs_paths.each do | path |
    describe file('/var/cache/lxc/' + path) do
      it { should be_directory }
      its('owner') { should eq 'root' }
      its('mode') { should cmp '00755' }
    end
  end
end
